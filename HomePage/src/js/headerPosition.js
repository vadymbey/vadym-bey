function headerPosition () {
    const header = document.querySelector('.home-page');
    
    document.addEventListener('wheel', (e) => {
        if (e.wheelDeltaY <= 0) {
            header.classList.add('active');
        } else {
            header.classList.remove('active');
            header.classList.add('scrolling');
            if (window.scrollY == 0) {
                header.classList.remove('scrolling');
            }
        }
    })
}

function mobileMenu () {
    const hederMenu = document.querySelector('.header-menu');
    const menu = document.querySelector('.header__mobile-icon');

    menu.addEventListener('click', (e) => {
        hederMenu.classList.toggle('active');
    })
}

headerPosition();
mobileMenu();