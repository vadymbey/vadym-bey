var panelItem = document.querySelectorAll('.panel-title'),
    bodyItem = document.querySelectorAll('.panel-body');
    panelItem.__proto__.forEach = [].__proto__.forEach;

var activePanel;
panelItem.forEach(function(item, i, panelItem) {
  item.addEventListener('click', function(e) {
    
    this.classList.add('panel-active');
    this.nextElementSibling.classList.add('active');

    if (activePanel) {
      activePanel.classList.remove('panel-active');
      activePanel.nextElementSibling.classList.remove('active');
    }

    activePanel = (activePanel === this) ? 0 : this;
  });
});