//Подключение модулей
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const babel = require('gulp-babel');
const browserify = require('gulp-browserify');

//Порядок подключения css файлов
const cssFiles = [
    './src/css/main.scss',
    './src/css/media.scss'
];

//Порядок подключения js файлов
const jsFiles = [
    './src/js/slider.js',
    './src/js/tabs.js',
    './src/js/headerPosition.js',
    './src/js/accordion.js',
];

//Таск на стиди CSS
function styles() {
    //Шаблон для поиска файлов CSS
    return gulp.src(cssFiles)
    .pipe(sass().on('error', sass.logError))
    //Объединение файлов в один
    .pipe(concat('style.css'))
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(cleanCSS({
        level: 2
    }))
    //Выходная папка для стилей
    .pipe(gulp.dest('./build/css'))
    .pipe(browserSync.stream());
}

//Таск на скрипты JS
function scripts() {
    //Шаблон для поиска файлов JS
    return gulp.src(jsFiles)
    //Объединение файлов в один
    .pipe(concat('script.js'))
    //Минификация JS
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(browserify({
        insertGlobals : true,
        // debug : !gulp.env.production
    }))
    .pipe(uglify({
        toplevel: true
    }))
    //Выходная папка для скриптов
    .pipe(gulp.dest('./build/js'))
    .pipe(browserSync.stream());
}

//Удалить все в указанной папке
function clean () {
    return del(['build/*']);
}

function optimize_image () {
    return gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('build/img'))
}

//Просматривать файлы
function watch() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    //Следить за CSS файлами
    gulp.watch('./src/css/**/*.scss', styles)
    //Следить за JS файлами
    gulp.watch('./src/js/**/*.js', scripts)
    //Следить за image файлами
    gulp.watch('./src/img/**/*', optimize_image)
    //При изменении  HTML запускать синхронизацию
    gulp.watch("./*.html").on('change', browserSync.reload);
}

//Таск вызывающий функцию styles
gulp.task('styles', styles);

//Таск вызывающий функцию scripts
gulp.task('scripts', scripts);

//Таск для очистки папки build
gulp.task('del', clean);

gulp.task('optimize_image', optimize_image);

//Таск отслеживания изменений
gulp.task('watch', watch);

// //Таск для удаления файлов в папке build и запуск styles и scripts
gulp.task('build', gulp.series(clean, optimize_image, gulp.parallel(styles,scripts)));
//Таск запускает таск build и watch последовательно
gulp.task('dev', gulp.series('build', 'watch'));